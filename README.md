# validacioUF1

Aquí trobareu la plantilla per la realització de la validació d'avui. L'entrega és via Moodle.

## Imatges

* Contingut mínim imatges:
	* Servidor web
	* Nom mantainer

## Compose

Inicialització a tots els contenidors i que tots treballin en un sola xarxa controlada.

## Scripts

Totes les comandes utilitzades a cada contenidor.